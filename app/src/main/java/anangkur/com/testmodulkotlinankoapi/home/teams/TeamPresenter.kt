package anangkur.com.testmodulkotlinankoapi.home.teams

import anangkur.com.testmodulkotlinankoapi.api.ApiRepository
import anangkur.com.testmodulkotlinankoapi.api.TheSportsDBApi
import anangkur.com.testmodulkotlinankoapi.model.TeamByLeagueResponse
import com.google.gson.Gson
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class TeamPresenter(private val view: TeamView, private val apiRepository: ApiRepository, private val gson: Gson){
    fun getTeamList(league: String){
        view.showLoading()
        doAsync {
            val data = gson.fromJson(apiRepository.doRequest(
                TheSportsDBApi.getTeams(
                    league
                )
            ), TeamByLeagueResponse::class.java)
            uiThread {
                view.hideLoading()
                view.showTeamList(data.teams)
            }
        }
    }
}