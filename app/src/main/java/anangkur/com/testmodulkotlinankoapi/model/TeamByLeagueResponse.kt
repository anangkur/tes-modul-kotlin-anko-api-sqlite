package anangkur.com.testmodulkotlinankoapi.model

data class TeamByLeagueResponse(
    val teams: List<Team>
)